variable "token"     {}
variable "folder_id" {}
variable "cloud_id"  {}
variable "zone"      {default = "ru-central1-a"}
variable "scale"     {default = 1}

terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.49.0"
    }
  }
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}
