data "yandex_compute_image" "nat-instance" {
  family = "nat-instance-ubuntu"
}

data "yandex_compute_image" "container-optimized-image" {
  family = "container-optimized-image"
}
