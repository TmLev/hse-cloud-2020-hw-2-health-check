# 2-health-check

Spin up `N` replicas, database, and balancer with
```schell script
docker-compose up --build --scale health-checker=N
```
