use serde::{Serialize, Deserialize};

use chrono::Utc;

use crate::models::Heartbeat;
use crate::SERVICE_CONFIG;

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct Success {
    pub ip: String,
    pub services: Vec<ServiceStatus>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Error {
    pub details: String,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ServiceStatus {
    pub ip: String,
    pub status: Status,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Status {
    Available,
    NotAvailable,
}

impl ServiceStatus {
    pub fn from(h: Heartbeat) -> Self {
        use Status::*;

        let bound = SERVICE_CONFIG.not_available_after_secs as i64;
        let status = if (Utc::now() - h.last).num_seconds() <= bound {
            Available
        } else {
            NotAvailable
        };

        ServiceStatus { ip: h.ip, status }
    }
}
