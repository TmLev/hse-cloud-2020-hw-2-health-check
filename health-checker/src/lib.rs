pub mod config;
pub mod models;
pub mod repo;
pub mod proto;

#[macro_use]
extern crate lazy_static;

use crate::config::{PostgresConfig, ServiceConfig};

lazy_static! {
    pub static ref POSTGRES_CONFIG: PostgresConfig = PostgresConfig::new();
    pub static ref SERVICE_CONFIG: ServiceConfig = ServiceConfig::new();
}
