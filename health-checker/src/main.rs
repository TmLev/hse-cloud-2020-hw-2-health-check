use actix_web::{App, HttpServer};

use dotenv::dotenv;

use log::info;

use health_check::repo;
use health_check::{POSTGRES_CONFIG, SERVICE_CONFIG};

use crate::service::{health_check_service, heartbeat};

mod migrate;
mod migrations;
mod service;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Configure

    dotenv().ok();
    env_logger::init();
    info!("Service config: {:?}", *SERVICE_CONFIG);
    info!("Postgres config: {:?}", *POSTGRES_CONFIG);

    // Setup database

    migrate::run_migrations().await;
    let repo = repo::PgRepo::new();
    let also_repo = repo.clone();

    // Spawn services

    actix_rt::spawn(async move { heartbeat(repo.clone()).await });
    HttpServer::new(move || App::new().data(also_repo.clone()).service(health_check_service))
        .bind(format!("0.0.0.0:{}", &SERVICE_CONFIG.port))?
        .run()
        .await
}
