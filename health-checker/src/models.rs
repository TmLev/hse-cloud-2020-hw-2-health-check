use sqlx::FromRow;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(FromRow, Serialize, Deserialize, Debug)]
pub struct Heartbeat {
    pub ip: String,
    pub last: DateTime<Utc>,
}
