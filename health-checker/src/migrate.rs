use std::time::Duration;

use actix_rt::time;

use refinery::config::{Config, ConfigDbType};

use log::{info, error};

use health_check::{POSTGRES_CONFIG, SERVICE_CONFIG};

use crate::migrations;

pub async fn run_migrations() {
    let mut conn = Config::new(ConfigDbType::Postgres)
        .set_db_user(&POSTGRES_CONFIG.user)
        .set_db_pass(&POSTGRES_CONFIG.password)
        .set_db_host(&POSTGRES_CONFIG.host)
        .set_db_port(&POSTGRES_CONFIG.port)
        .set_db_name(&POSTGRES_CONFIG.db);

    let retry = Duration::from_secs(SERVICE_CONFIG.run_migrations_retry_secs);
    let mut interval = time::interval(retry);

    loop {
        info!("Running migrations");

        // TODO: synchronous due to `refinery` and `sqlx` incompatibility
        if let Err(e) = migrations::runner().run(&mut conn) {
            error!("Failed to run migrations: {}", e);
        } else {
            info!("Migrations run successfully");
            return;
        }

        info!("Sleeping before next attempt");
        interval.tick().await;
    }
}
