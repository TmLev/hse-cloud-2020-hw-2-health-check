use barrel::backend::Pg;
use barrel::{types, Migration};

pub fn migration() -> String {
    let mut m = Migration::new();

    m.create_table("heartbeats", |t| {
        t.add_column("ip", types::text().primary(true).unique(true));
        t.add_column("last", types::custom("TIMESTAMPTZ"));
    });

    m.make::<Pg>()
}
