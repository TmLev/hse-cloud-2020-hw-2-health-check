use std::time::Duration;

use chrono::Utc;
use sqlx::{
    postgres::{PgDone, PgPoolOptions},
    Error, PgPool,
};

use crate::models::Heartbeat;
use crate::{POSTGRES_CONFIG, SERVICE_CONFIG};

#[derive(Clone)]
pub struct PgRepo {
    pool: PgPool,
}

impl PgRepo {
    pub fn new() -> Self {
        let max_connections = SERVICE_CONFIG.max_db_connections;
        let timeout = Duration::from_millis(SERVICE_CONFIG.db_timeout_millis);
        let url = &POSTGRES_CONFIG.url;

        let pool = PgPoolOptions::new()
            .max_connections(max_connections)
            .connect_timeout(timeout)
            .connect_lazy(url)
            .expect("Failed to parse Postgres URL");

        PgRepo { pool }
    }

    pub async fn list_heartbeats(&self) -> Result<Vec<Heartbeat>, String> {
        sqlx::query_as("SELECT * FROM heartbeats")
            .fetch_all(&self.pool)
            .await
            .map_err(stringify)
    }

    pub async fn heartbeat(&self, ip: &str) -> Result<PgDone, String> {
        sqlx::query(
            "INSERT INTO heartbeats (ip, last) VALUES ($1, $2)
             ON CONFLICT (ip) DO UPDATE SET last = $2"
        )
            .bind(ip)
            .bind(Utc::now())
            .execute(&self.pool)
            .await
            .map_err(stringify)
    }
}

fn stringify(e: Error) -> String {
    match e {
        Error::PoolTimedOut => "Database connection timeout.".to_string(),
        Error::Io(_) => "Failed to communicate with database.".to_string(),
        Error::Database(e) => e.to_string(),
        _ => "Unhandled generic error.".to_string(),
    }
}
