use std::time::Duration;

use reqwest::blocking::ClientBuilder;

use config::{Config, Environment};
use serde::Deserialize;

use log::info;

#[derive(Deserialize, Clone, Debug)]
pub struct PostgresConfig {
    pub user: String,
    pub password: String,
    pub host: String,
    pub port: String,
    pub db: String,
    pub url: String,
}

impl PostgresConfig {
    pub fn new() -> Self {
        let mut config = Config::new();

        config
            .merge(Environment::with_prefix("postgres"))
            .expect("Failed to merge variables from environment");

        config
            .set(
                "url",
                format!(
                    "postgres://{}:{}@{}:{}/{}",
                    config.get_str("user").unwrap(),
                    config.get_str("password").unwrap(),
                    config.get_str("host").unwrap(),
                    config.get_str("port").unwrap(),
                    config.get_str("db").unwrap(),
                ),
            )
            .unwrap();

        config.try_into().expect("Failed to deserialize config")
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct ServiceConfig {
    pub ip: String,
    pub port: String,

    pub run_migrations_retry_secs: u64,
    pub max_db_connections: u32,
    pub db_timeout_millis: u64,

    pub send_heartbeat_every_secs: u64,
    pub not_available_after_secs: u64,
}

impl ServiceConfig {
    pub fn new() -> Self {
        let mut config = Config::new();

        config
            .merge(Environment::with_prefix("service"))
            .expect("Failed to merge variables from environment");

        config.set("ip", discover_ip()).unwrap();

        config.try_into().expect("Failed to deserialize config")
    }
}

fn discover_ip() -> String {
    let url = "http://169.254.169.254/latest/meta-data/local-ipv4";

    let timeout = Duration::new(1, 0);
    let client = ClientBuilder::new().danger_accept_invalid_certs(true).timeout(timeout).build().unwrap();
    let response = client.get(url).send();

    let ip = match response {
        Err(e) => {
            info!("Failed to fetch ip: {}", e.to_string());
            std::env::var("SERVICE_IP").unwrap_or("IP_FOR_TEST".to_string())
        }
        Ok(r) => {
            info!("Successfully fetched ip");
            r.text().unwrap()
        }
    };

    info!("Using ip: {}", ip);

    ip
}
