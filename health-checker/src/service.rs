use std::time::Duration;

use actix_rt::time;
use actix_web::{get, web, HttpResponse, Responder};

use log::{info, debug, error};

use health_check::repo::PgRepo;
use health_check::proto::{ServiceStatus, Success, Error};
use health_check::SERVICE_CONFIG;

pub async fn heartbeat(repo: PgRepo) -> ! {
    let seconds = Duration::from_secs(SERVICE_CONFIG.send_heartbeat_every_secs);
    let mut interval = time::interval(seconds);

    loop {
        debug!("Sleeping for {:?}", seconds);
        interval.tick().await;

        debug!("Sending heartbeat");
        if let Err(e) = repo.heartbeat(&SERVICE_CONFIG.ip).await {
            error!("Failed to send heartbeat: {}", e);
        }
    }
}

#[get("/health-check")]
pub async fn health_check_service(repo: web::Data<PgRepo>) -> impl Responder {
    info!("Received health_check request");

    match repo.list_heartbeats().await {
        Err(details) => HttpResponse::ServiceUnavailable().json(Error { details }),
        Ok(heartbeats) => HttpResponse::Ok().json(Success {
            ip: SERVICE_CONFIG.ip.clone(),
            services: heartbeats.into_iter().map(ServiceStatus::from).collect(),
        }),
    }
}
