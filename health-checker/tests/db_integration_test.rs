use std::time::Duration;

use actix_rt::time;

use reqwest::blocking::ClientBuilder;

use health_check::proto::Success;
use health_check::repo::PgRepo;
use health_check::SERVICE_CONFIG;

mod common;

#[test]
fn services_are_available() {
    common::setup();

    let timeout = Duration::new(5, 0);
    let client = ClientBuilder::new()
        .danger_accept_invalid_certs(true)
        .timeout(timeout)
        .build()
        .unwrap();

    let url = format!("http://health-checker-1:{}/health-check", SERVICE_CONFIG.port);
    // let url = format!("http://0.0.0.0:11000/health-check");
    let response = client.get(&url).send().unwrap();

    let ips = common::sorted_service_ips();
    let expected = format!(r#"
        {{
          "ip": "{ip_0}",
          "services": [
            {{
              "ip": "{ip_0}",
              "status": "AVAILABLE"
            }},
            {{
              "ip": "{ip_1}",
              "status": "AVAILABLE"
            }}
          ]
        }}"#, ip_0 = ips[0], ip_1 = ips[1]);
    let mut expected: Success = serde_json::from_str(&expected).unwrap();
    expected.services.sort_by(|left, right| left.ip.cmp(&right.ip));

    let mut got: Success = response.json().unwrap();
    got.services.sort_by(|left, right| left.ip.cmp(&right.ip));

    assert_eq!(expected, got);
}

#[actix_rt::test]
async fn heartbeats_table_updates() {
    common::setup();

    let repo = PgRepo::new();

    let mut first: Vec<_> = repo.list_heartbeats()
        .await
        .unwrap()
        .iter()
        .map(|h| h.last)
        .collect();
    first.sort();

    let tick = SERVICE_CONFIG.send_heartbeat_every_secs + 1;
    let mut interval = time::interval(Duration::from_secs(tick));
    interval.tick().await; // nop for the first time
    interval.tick().await;

    let mut second: Vec<_> = repo.list_heartbeats()
        .await
        .unwrap()
        .iter()
        .map(|h| h.last)
        .collect();
    second.sort();

    for (f, s) in first.iter().zip(second.iter()) {
        assert!(f < s);
    }
}
