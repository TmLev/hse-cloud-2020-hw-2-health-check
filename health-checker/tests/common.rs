use dotenv::from_filename;

pub fn setup() {
    from_filename(".env.test").ok();
}

// SERVICE_IPS=11.0.0.0,11.0.0.1
pub fn sorted_service_ips() -> Vec<String> {
    let mut ips:Vec<String> = std::env::var("SERVICE_IPS")
        .unwrap()
        .split(',')
        .map(str::to_string)
        .collect();

    ips.sort();

    ips
}
