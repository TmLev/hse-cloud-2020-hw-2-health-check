use std::time::Duration;

use reqwest::blocking::ClientBuilder;

use health_check::proto::Success;

mod common;

#[test]
fn round_robin_balancer() {
    common::setup();

    let timeout = Duration::new(5, 0);
    let client = ClientBuilder::new()
        .danger_accept_invalid_certs(true)
        .timeout(timeout)
        .build()
        .unwrap();

    let url = "http://balancer/health-check";

    let mut response_ips: Vec<String> = vec![];

    for _ in 0..2 {
        let response = client.get(url).send().unwrap();
        let response: Success = response.json().unwrap();
        response_ips.push(response.ip);
    }

    response_ips.sort();

    assert_eq!(response_ips, common::sorted_service_ips());
}
